﻿using GoodMobile.Models.Models;
using GoodMoblie.DAL.Context;
using GoodMoblie.DAL.Repository.Contract;

namespace GoodMoblie.DAL.Repository
{
    public class SpecificationRepository : GenericRepository<Specification>, ISpecificationRepository
    {
        private readonly ApplicationDbContext _db;

        public SpecificationRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}