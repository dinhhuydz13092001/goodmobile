﻿using GoodMobile.Models.Models;
using GoodMoblie.DAL.Context;
using GoodMoblie.DAL.Repository.Contract;

namespace GoodMoblie.DAL.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly ApplicationDbContext _db;

        public ProductRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
