﻿using GoodMobile.Models.Models;
using GoodMoblie.DAL.Context;
using GoodMoblie.DAL.Repository.Contract;

namespace GoodMoblie.DAL.Repository
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private readonly ApplicationDbContext _db;
        public CategoryRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
