﻿using GoodMobile.Models.Models;

namespace GoodMoblie.DAL.Repository.Contract
{
    public interface ISpecificationRepository : IGenericRepository<Specification>
    {
    }
}
