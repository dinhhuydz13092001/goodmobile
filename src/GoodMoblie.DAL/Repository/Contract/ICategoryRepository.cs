﻿using GoodMobile.Models.Models;

namespace GoodMoblie.DAL.Repository.Contract
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
