﻿using GoodMobile.Models.Models;

namespace GoodMoblie.DAL.Repository.Contract
{
    public interface IImageRepository : IGenericRepository<Image>
    {
    }
}