﻿namespace GoodMoblie.DAL.Repository.Contract
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Product { get; }
        ICategoryRepository Category { get; }
        ISpecificationRepository Specification { get; }
        IImageRepository Image { get; }
        void Save();
    }
}
