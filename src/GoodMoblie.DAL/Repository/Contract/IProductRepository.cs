﻿using GoodMobile.Models.Models;

namespace GoodMoblie.DAL.Repository.Contract
{
    public interface IProductRepository : IGenericRepository<Product>
    {
    }
}
