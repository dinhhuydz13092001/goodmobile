﻿using GoodMobile.Models.Models;
using GoodMoblie.DAL.Context;
using GoodMoblie.DAL.Repository.Contract;

namespace GoodMoblie.DAL.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Product = new ProductRepository(_context);
            Category = new CategoryRepository(_context);
            Specification = new SpecificationRepository(_context);
            Image = new ImageRepository(_context);
        }

        public IProductRepository Product { get; private set; }

        public ICategoryRepository Category { get; private set; }

        public ISpecificationRepository Specification { get; private set; }

        public IImageRepository Image { get; private set; }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
