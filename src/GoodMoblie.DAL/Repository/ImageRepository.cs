﻿using GoodMobile.Models.Models;
using GoodMoblie.DAL.Context;
using GoodMoblie.DAL.Repository.Contract;

namespace GoodMoblie.DAL.Repository
{
    internal class ImageRepository : GenericRepository<Image>, IImageRepository
    {
        private readonly ApplicationDbContext _db;

        public ImageRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}