﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GoodMoblie.DAL.Migrations
{
    public partial class Edit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Specification_Product_ProductId",
                table: "Specification");

            migrationBuilder.DropIndex(
                name: "IX_Specification_ProductId",
                table: "Specification");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "Specification");

            migrationBuilder.AddColumn<string>(
                name: "GenerationName",
                table: "Specification",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "GenerationName",
                table: "Product",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Price",
                table: "Product",
                type: "int",
                maxLength: 100,
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GenerationName",
                table: "Specification");

            migrationBuilder.DropColumn(
                name: "GenerationName",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "Product");

            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "Specification",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Specification_ProductId",
                table: "Specification",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Specification_Product_ProductId",
                table: "Specification",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
