﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GoodMoblie.DAL.Migrations
{
    public partial class Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Specification_SpecificationId",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_SpecificationId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "SpecificationId",
                table: "Product");

            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "Specification",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Specification_ProductId",
                table: "Specification",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Specification_Product_ProductId",
                table: "Specification",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Specification_Product_ProductId",
                table: "Specification");

            migrationBuilder.DropIndex(
                name: "IX_Specification_ProductId",
                table: "Specification");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "Specification");

            migrationBuilder.AddColumn<int>(
                name: "SpecificationId",
                table: "Product",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Product_SpecificationId",
                table: "Product",
                column: "SpecificationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Specification_SpecificationId",
                table: "Product",
                column: "SpecificationId",
                principalTable: "Specification",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
