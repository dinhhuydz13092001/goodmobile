﻿namespace GoodMobile.MVC.Models.ViewModel
{
    public class SimilarProductViewModel
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public string Price { get; set; }
        public string Image {  get; set; }
    }
}
