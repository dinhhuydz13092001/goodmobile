﻿namespace GoodMobile.MVC.Models.ViewModel
{
    public class SpecificationViewModel
    {
        public string Sim { get; set; }
        public string Design { get; set; }
        public string Display { get; set; }
        public string Resolution { get; set; }
        public string Pin { get; set; }
        public string CPU { get; set; }
        public string RAM { get; set; }
        public string ROM { get; set; }
        public string FrontCamera { get; set; }
        public string RearCamera { get; set; }
    }
}
