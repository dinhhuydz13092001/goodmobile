﻿namespace GoodMobile.MVC.Models.ViewModel
{
    public class ProductViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string CategoryName { get; set; }
        public List<string> images { get; set; } = new List<string>();
        public SpecificationViewModel specification { get; set; }
    }
}
