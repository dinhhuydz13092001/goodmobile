﻿using GoodMobile.Models.Models;
using GoodMobile.MVC.Helper;
using GoodMobile.MVC.Models.ViewModel;
using GoodMoblie.DAL.Repository.Contract;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Expressions;

namespace GoodMobile.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly int pageSize = 8;
        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IActionResult Index(int? P, string fillter, string order)
        {
            Expression<Func<Product,bool>> fillterQuery = null;
            Func<IQueryable<Product>, IOrderedQueryable<Product>> orderQuery = null;
            if(!string.IsNullOrEmpty(order))
            {
                if(order == "name")
                {
                    orderQuery = o => o.OrderBy(p => p.Name);
                    ViewBag.Order = "name";
                }
                else if(order == "price-asc")
                {
                    orderQuery = o => o.OrderBy(p => p.Price);
                    ViewBag.Order = "price-asc";
                }
                else if (order == "price-desc")
                {
                    orderQuery = o => o.OrderByDescending(p => p.Price);
                    ViewBag.Order = "price-desc";
                }
                
            }

            if(fillter != null && int.TryParse(fillter, out int cateId))
            {
                fillterQuery = f => (f.Category.Id == cateId);
                ViewBag.fillter = fillter;
                ViewBag.Category = _unitOfWork.Category.GetByID(cateId).Name;
            }


            var products = _unitOfWork.Product.Get(fillterQuery, orderQuery, "");

            List<SimilarProductViewModel> productVMs = new List<SimilarProductViewModel>();
            Image image = null;
            if (products.Any())
            {
                foreach (var p in products)
                {
                    image = _unitOfWork.Image.Get(x => x.Product.Id == p.Id).First();
                    productVMs.Add(new SimilarProductViewModel()
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Price = p.Price.ToStringPrice(),
                        Image = StringHelper.GetImgPath(p.GenerationName, image.Path)
                    });
                }
            }

            return View(PaginatedList<SimilarProductViewModel>.Create(productVMs, P ?? 1, pageSize));
        }
    }
}