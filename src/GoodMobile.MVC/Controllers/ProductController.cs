﻿using GoodMobile.Models.Models;
using GoodMobile.MVC.Helper;
using GoodMobile.MVC.Models.ViewModel;
using GoodMoblie.DAL.Repository.Contract;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Expressions;

namespace GoodMobile.MVC.Controllers
{
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Route("Product/{id}")]
        public IActionResult DetailProduct(int id)
        {
            Product? p = _unitOfWork.Product.Get(x=>x.Id == id, null, "Category").First();
            Specification? specification = _unitOfWork.Specification.Get(x=>x.GenerationName ==p.GenerationName).FirstOrDefault();
            List<Image> images = _unitOfWork.Image.Get(x => x.Product.Id == id).ToList();
            var pVM = new ProductViewModel()
            {
                Name = p.Name,
                Description = p.Description,
                Price = p.Price.ToStringPrice(),
                CategoryName = p.Category.Name,
               

                specification = new SpecificationViewModel()
                {
                    Sim = specification.Sim,
                    Design = specification.Design,
                    Display = specification.Display,
                    Resolution = specification.Resolution,
                    Pin = specification.Pin,
                    CPU = specification.CPU,
                    RAM = specification.RAM,
                    ROM = specification.ROM,
                    FrontCamera = specification.FrontCamera,
                    RearCamera  = specification.RearCamera,
                }
            };
            
            string imgPath = string.Empty;
            foreach (var image in images)
            {
                imgPath = StringHelper.GetImgPath(p.GenerationName, image.Path);
                pVM.images.Add(imgPath);
            }

            var similarProduct = _unitOfWork.Product.Get(x => x.Category.Id == p.Category.Id);
            List<SimilarProductViewModel> similarProductViewModels = new List<SimilarProductViewModel>();
            Image similarImage = null;
            if (similarProduct.Any())
            {
                if(similarProduct.Count() > 4)
                {
                    similarProduct = similarProduct.Take(4);
                }
                foreach (var product in similarProduct)
                {
                    similarImage = _unitOfWork.Image.Get(x => x.Product.Id == product.Id).First();   
                    similarProductViewModels.Add(new SimilarProductViewModel() { 
                        Id = product.Id,
                        Name = product.Name,
                        Price = product.Price.ToStringPrice(),
                        Image = StringHelper.GetImgPath(product.GenerationName, similarImage.Path)
                    });
                }
                ViewBag.SimilarProductVM = similarProductViewModels;
            }

            return View(pVM);
        }
    }
}