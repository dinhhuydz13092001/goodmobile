﻿using GoodMobile.Models.Models;
using GoodMoblie.DAL.Repository.Contract;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Expressions;

namespace GoodMobile.MVC.Controllers
{
    public class CategoryController : Controller
    {
        IUnitOfWork _unitOfWork;
        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Route("Collections/{id}")]
        public IActionResult Index(int id)
        {
            Expression<Func<Product,bool>> fillter = x=>x.Category.Id == id;
            var products = _unitOfWork.Product.Get(fillter, null, "Category");

            return View();
        }
    }
}
