﻿using GoodMobile.Models.Models;

namespace GoodMobile.MVC.Helper
{
    public static class StringHelper
    {
        public static string ToStringPrice(this int price) { 
            string priceToString = price.ToString();
            priceToString = priceToString.Insert(priceToString.Length - 3, ",");
            priceToString = priceToString.Insert(priceToString.Length, ",000₫");
            return priceToString;
        }

        public static string GetImgPath(string generationName, string imageName) {
            return $"/Image/Prd/{generationName}/{imageName}";
        }

        public static string GetPhoneVersionName(string generationName)
        {
            if (generationName == "X")
                return "IPhone X";
            else if (generationName == "XR")     
                return "IPhone XR";
            else if (generationName == "XS")
                return "IPhone XS";
            else if (generationName == "XSM")
                return "IPhone XSM";
            else if (generationName == "11")
                return "IPhone 11";
            else if (generationName == "11PR")
                return "IPhone 11 Pro";
            else if (generationName == "11PRM")
                return "IPhone 11 Pro Max";
            else if (generationName == "12")
                return "IPhone 12";
            else if (generationName == "12PR")
                return "IPhone 12 Pro";
            else if (generationName == "12PRM")
                return "IPhone 12 Pro Max";
            else if (generationName == "13")
                return "IPhone 13";
            else if (generationName == "13PR")
                return "IPhone 13 Pro";
            else if (generationName == "13PRM")
                return "IPhone 13 Pro Max";
            else if (generationName == "14")
                return "IPhone 14";
            else if (generationName == "14P")
                return "IPhone 14 Plus";
            else if (generationName == "14PR")
                return "IPhone 14 Pro";
            else if (generationName == "14PRM")
                return "IPhone 14 Pro Max";
            else
                return "Android";
        }

    }
}
