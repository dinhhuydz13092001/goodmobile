﻿function ChangeImg(e) {
    const mainImg = document.getElementById("mainImg");
    mainImg.src = e.currentTarget.src;
}

function GoToProduct(id) {
    window.location.href = "/Product/" + id;
}