﻿
function Paginated() {
    document.querySelectorAll('.btn-paginated').forEach(button => {
        button.addEventListener('click', function () {
            var value = this.innerText;
            const fillter = document.getElementById("is-fillter").innerText;
            const order = document.getElementById("is-order").innerText;
            let url = "/?p=" + value;
            if (order != '') {
                url += "&order=" + order;
            }
            if (fillter != '') {
                url += "&fillter=" + fillter;
            }
            window.location.href = url;
        });
    });
}

Paginated();

function ChangeColorPagenatedButton() {
    const pageIndex = document.getElementById("spn-currenPage").innerText;

    document.querySelectorAll('.btn-paginated').forEach(button => {
        if (button.innerText == pageIndex) {
            button.classList.remove("btn-outline-primary");
            button.classList.add("btn-primary");
        }
    });

    
}

ChangeColorPagenatedButton();

function GoToProduct(id) {
    window.location.href = "/Product/" + id;
}

function PreviousPage(currentPage) {
    if (currentPage > 1) {
        const fillter = document.getElementById("is-fillter").innerText;
        const order = document.getElementById("is-order").innerText;
        let url = "/?p=" + (currentPage - 1);
        if (order != '') {
            url += "&order=" + order;
        }
        if (fillter != '') {
            url += "&fillter=" + fillter;
        }
        window.location.href = url;
    }
}

function NextPage(currentPage, totalPage) {
    if (currentPage < totalPage) {
        const fillter = document.getElementById("is-fillter").innerText;
        const order = document.getElementById("is-order").innerText;
        let url = "/?p=" + (currentPage + 1);
        if (order != '') {
            url += "&order=" + order;
        }
        if (fillter != '') {
            url += "&fillter=" + fillter;
        }
        window.location.href = url;
    }
}

function SoftOrder() {
    let mySelect = document.getElementById('order-select');
    const fillter = document.getElementById("is-fillter").innerText;
    mySelect.addEventListener('change', function () {
        let url = "/?p=1";
        if (this.value != 'default') {
            url += "&order=" + this.value
        }
        
        if (fillter != '') {
            url += "&fillter=" + fillter;
        }
        window.location.href = url;
    });
}

SoftOrder();

function SetOrderSelected() {
    const select = document.getElementById("order-select");
    const order = document.getElementById("is-order").innerText;
    if (order != '') {
        if (order == "name") {
            select.options[1].selected = true;
        }
        else if (order == "price-asc") {
            select.options[2].selected = true;
        }
        else if (order == "price-desc") {
            select.options[3].selected = true;
        }
        else {
            select.options[0].selected = true;
        }
    }
}

SetOrderSelected();
