/*!
* Start Bootstrap - Shop Homepage v5.0.6 (https://startbootstrap.com/template/shop-homepage)
* Copyright 2013-2023 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-shop-homepage/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project
function Fillter() {
    const order = document.getElementById("is-order").innerText;
    const category = document.querySelectorAll(".cate-item").forEach(button => {
        button.addEventListener('click', function () {
            let url = "/?p=1&fillter=" + this.value;
            if (order != '') {
                url += "&order=" + order;
            }
            window.location.href = url;
        });
    });
}

Fillter();