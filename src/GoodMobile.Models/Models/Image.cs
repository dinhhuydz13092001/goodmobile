﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GoodMobile.Models.Models
{
    public class Image
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        public string Path { get; set; }
    }
}
